#!/bin/bash

# -- colors
GREEN='\033[32m'
BLUE='\033[34m'
WHITE="\033[1;37m"
MAGENTA="\033[1;35m"
RED="\033[1;31m"
YELLOW="\033[1;33m"
CAYN="\033[1;36m"
BLACK="\033[1;40;30m"
RESET="\033[0m"
FG="\033[1;47;37m"
BGYELLOW="\033[1;43;33m"
BGWHITE="\033[1;47;37m"

distro_detect() {
	os="$(uname -o)"
	case "${os}" in
	Android)
		distro="Android"
		;;
	*)
		distro="$(source /etc/os-release && echo "${PRETTY_NAME}")"
		;;
	esac
}

# -- Kernel
get_kernel() {
    uname -r
}

# -- Memory 
get_mem() { 
  free -h | awk '/^Mem:/ {print $3 "/" $2}' | sed 's/i//g'
}

# -- Shell
get_shell() {
    env | awk '/^SHELL=/ {print $1}' | sed 's/SHELL=//g' | sed -e 's/bin//g' | sed -e 's/\///g'
}

# -- Terminal
get_term() {
    env | awk '/^TERM=/ {print $1}' | sed 's/TERM=//g'
}

# -- Window Manaegr
get_wm() {
    ((wm_run == 1)) && return

    case $kernel_name in
        *OpenBSD*) ps_flags=(x -c) ;;
        *)         ps_flags=(-e) ;;
    esac

    if [[ -O "${XDG_RUNTIME_DIR}/${WAYLAND_DISPLAY:-wayland-0}" ]]; then
        if tmp_pid="$(lsof -t "${XDG_RUNTIME_DIR}/${WAYLAND_DISPLAY:-wayland-0}" 2>&1)" ||
           tmp_pid="$(fuser   "${XDG_RUNTIME_DIR}/${WAYLAND_DISPLAY:-wayland-0}" 2>&1)"; then
            wm="$(ps -p "${tmp_pid}" -ho comm=)"
        else
            wm=$(ps "${ps_flags[@]}" | grep -m 1 -o -F \
                               -e arcan \
                               -e asc \
                               -e clayland \
                               -e dwc \
                               -e fireplace \
                               -e gnome-shell \
                               -e greenfield \
                               -e grefsen \
                               -e hikari \
                               -e kwin \
                               -e lipstick \
                               -e maynard \
                               -e mazecompositor \
                               -e motorcar \
                               -e orbital \
                               -e orbment \
                               -e perceptia \
                               -e river \
                               -e rustland \
                               -e sway \
                               -e ulubis \
                               -e velox \
                               -e wavy \
                               -e way-cooler \
                               -e wayfire \
                               -e wayhouse \
                               -e westeros \
                               -e westford \
                               -e weston)
        fi

    elif [[ $DISPLAY && $os != "Mac OS X" && $os != "macOS" && $os != FreeMiNT ]]; then
        wm=$(ps "${ps_flags[@]}" | grep -m 1 -o \
                           -e "[s]owm" \
                           -e "[c]atwm" \
                           -e "[f]vwm" \
                           -e "[d]wm" \
                           -e "[2]bwm" \
                           -e "[m]onsterwm" \
                           -e "[t]inywm" \
                           -e "[x]11fs" \
                           -e "[x]monad")

        [[ -z $wm ]] && type -p xprop &>/dev/null && {
            id=$(xprop -root -notype _NET_SUPPORTING_WM_CHECK)
            id=${id##* }
            wm=$(xprop -id "$id" -notype -len 100 -f _NET_WM_NAME 8t)
            wm=${wm/*WM_NAME = }
            wm=${wm/\"}
            wm=${wm/\"*}
        }

    else
        case $os in
            "Mac OS X"|"macOS")
                ps_line=$(ps -e | grep -o \
                    -e "[S]pectacle" \
                    -e "[A]methyst" \
                    -e "[k]wm" \
                    -e "[c]hun[k]wm" \
                    -e "[y]abai" \
                    -e "[R]ectangle")

                case $ps_line in
                    *chunkwm*)   wm=chunkwm ;;
                    *kwm*)       wm=Kwm ;;
                    *yabai*)     wm=yabai ;;
                    *Amethyst*)  wm=Amethyst ;;
                    *Spectacle*) wm=Spectacle ;;
                    *Rectangle*) wm=Rectangle ;;
                    *)           wm="Quartz Compositor" ;;
                esac
            ;;

            Windows)
                wm=$(
                    tasklist |
                    grep -Fom 1 \
                         -e bugn \
                         -e Windawesome \
                         -e blackbox \
                         -e emerge \
                         -e litestep
                )

                [[ $wm == blackbox ]] &&
                    wm="bbLean (Blackbox)"

                wm=${wm:+$wm, }DWM.exe
            ;;

            FreeMiNT)
                freemint_wm=(/proc/*)

                case ${freemint_wm[*]} in
                    *xaaes* | *xaloader*) wm=XaAES ;;
                    *myaes*)              wm=MyAES ;;
                    *naes*)               wm=N.AES ;;
                    geneva)               wm=Geneva ;;
                    *)                    wm="Atari AES" ;;
                esac
            ;;
        esac
    fi

    [[ $wm == *WINDOWMAKER* ]] && wm=wmaker
    [[ $wm == *GNOME*Shell* ]] && wm=Mutter

    wm_run=1
}
# -- Uptime
get_upt() {
    uptime --pretty | sed -e 's/up //g' -e 's/ days/d/g' -e 's/ day/d/g' -e 's/ hours/h/g' -e 's/ hour/h/g' -e 's/ minutes/m/g' -e 's/, / /g'
}

# -- Packges
get_pkg() {
	pack="$(which {xbps-install,apt,pacman} 2>/dev/null | grep -v "not found" | awk -F/ 'NR==1{print $NF}')"
	case "${pack}" in
	"xbps-install")
		total=$(xbps-query -l | wc -l)
		;;
	"apt")
		total=$(apt list --installed 2>/dev/null | wc -l)
		;;
	"pacman")
		total=$(pacman -Q | wc -l)
		;;
	"")
		total="Unknown"
		;;
	esac
	echo $total
}

# -- Init
get_init() {
	os="$(uname -o)"
	if [[ "$os" = "Android" ]]; then
		echo "init.rc"
	elif [[ ! $(pidof systemd) ]]; then
		if [[ -f "/sbin/openrc" ]]; then
			echo "openrc"
		else
			echo $(cat /proc/1/comm)
		fi
	else
		echo "systemD"
	fi
}

# -- Print
distro_detect
get_wm
echo -e "               ${GREEN}os${BLUE}        ~  ${WHITE}${distro} $(uname -m)"
echo -e "               ${GREEN}Kernel${BLUE}    ~  ${WHITE}$(get_kernel)"
echo -e "     ${WHITE}•${BLACK}_${WHITE}•${RESET}       ${GREEN}Uptime${BLUE}    ~  ${WHITE}$(get_upt)"
echo -e "     ${BLACK}${RESET}${BGYELLOW}oo${RESET}${BLACK}|${RESET}       ${GREEN}pkgs${BLUE}      ~  ${WHITE}$(get_pkg)"
echo -e "    ${BLACK}/${RESET}${BGWHITE} ${RESET}${BLACK}'\'${RESET}      ${GREEN}Shell${BLUE}     ~  ${WHITE}$(get_shell)"
echo -e "   ${BGYELLOW}(${RESET}${BLACK}\_;/${RESET}${BGYELLOW})${RESET}      ${GREEN}WM${BLUE}        ~  ${WHITE}${wm}"
echo -e "               ${GREEN}Terminal${BLUE}  ~  ${WHITE}$(get_term)"
echo -e "               ${GREEN}Memory${BLUE}    ~  ${WHITE}$(get_mem)"
echo -e "               ${GREEN}Init${BLUE}      ~  ${WHITE}$(get_init)"
echo -e ""
echo -e "      ${YELLOW}██  ${YELLOW}${RED}██  ${RED}${GREEN}██  ${GREEN}${BLUE}██  ${BLUE}${CAYN}██  ${CAYN}${MAGENTA}██  ${MAGENTA}${WHITE}██  ${WHITE}"
echo -e ""
